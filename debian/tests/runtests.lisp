(require "asdf")

; Until the testsuite is really exercised (see #922311), the tests are marked
; “superficial” in debian/tests/control

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  (asdf:load-system "cl-fad"))
